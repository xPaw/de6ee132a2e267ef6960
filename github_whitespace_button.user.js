// ==UserScript==
// @name           Ignore whitespace button on GitHub
// @description    Adds a button in GitHub UI to ignore whitespace changes in commits
// @author         xPaw
// @namespace      https://gist.github.com/xPaw/de6ee132a2e267ef6960
// @updateURL      https://gist.github.com/xPaw/de6ee132a2e267ef6960/raw/github_whitespace_button.user.js
// @match          https://github.com/*
// @version        1.6.1
// @run-at         document-end
// @grant          none
// ==/UserScript==

function AddWhitespaceButton()
{
	var hasWhitespaceSwitch = window.location.search.match( /[\?&]w=/ ) !== null;
	
	AddButtonInPullRequestReview( hasWhitespaceSwitch );
	AddButtonInCommits( hasWhitespaceSwitch );
}

function AddButtonInPullRequestReview( hasWhitespaceSwitch )
{
	var toc = document.querySelector( '.pr-review-tools' );
	
	if( !toc || document.getElementById( 'js-xpaw-whitespace-button' ) )
	{
		return;
	}
	
	var buttonContainer = document.createElement( 'div' );
	buttonContainer.className = 'diffbar-item';
	
	var button = CreateButton( hasWhitespaceSwitch );
	
	buttonContainer.appendChild( button );
	toc.prepend( buttonContainer );
}

function AddButtonInCommits( hasWhitespaceSwitch )
{
	var toc = document.querySelector( '#toc > .BtnGroup' );
	
	if( !toc || document.getElementById( 'js-xpaw-whitespace-button' ) )
	{
		return;
	}
	
	var button = CreateButton( hasWhitespaceSwitch );
	button.className += ' float-right';
	button.style.marginRight = '6px';
	
	toc.parentNode.insertBefore( button, toc.nextSibling );
}

function CreateButton( hasWhitespaceSwitch )
{
	var button = document.createElement( 'a' );
	button.id = 'js-xpaw-whitespace-button';
	button.textContent = 'Whitespace';
	button.className = 'btn btn-sm btn-outline' + ( hasWhitespaceSwitch ? ' selected' : '' );
	button.href = UpdateQueryString( 'w', hasWhitespaceSwitch ? null : '1' );
	button.dataset.pjax = '#js-repo-pjax-container';
	
	return button;
}

AddWhitespaceButton();

document.addEventListener( 'pjax:complete', AddWhitespaceButton );

// http://stackoverflow.com/a/11654596/2200891
function UpdateQueryString(key, value)
{
	var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
		url = window.location.href,
		hash;
	
	if (re.test(url))
	{
		if (typeof value !== 'undefined' && value !== null)
		{
			url = url.replace(re, '$1' + key + "=" + value + '$2$3');
		}
		else
		{
			hash = url.split('#');
			url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
			
			if (typeof hash[1] !== 'undefined' && hash[1] !== null)
			{
				url += '#' + hash[1];
			}
		}
	}
	else if (typeof value !== 'undefined' && value !== null)
	{
		var separator = url.indexOf('?') !== -1 ? '&' : '?';
		hash = url.split('#');
		url = hash[0] + separator + key + '=' + value;
		
		if (typeof hash[1] !== 'undefined' && hash[1] !== null)
		{
			url += '#' + hash[1];
		}
	}
	
	return url;
}
